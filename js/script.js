
const tabs = document.querySelector('.tabs');
tabs.addEventListener('click', (event) => {
  document.querySelector('.active').classList.remove('active')
  event.target.classList.add('active')
  document.querySelector('.tabs-content .active ').classList.remove('active')
  const data = event.target.getAttribute('data-tab')
  console.log(data);
  document.querySelector(`.tabs-content [data-tab="${data}"]`).classList.add('active')
});